# Lightcurve Simulator #
### Created by Evan Anders for PDP Group 25, 2017 ###

This repository contains code for creating simple light curves of planets orbiting single stars.  "Spotty" stars with sunspots are also able to be simulated.

## Dependencies ##
In order to run this code, you must have the following dependencies installed:

1. Python version 3.x, with...
2. Matplotlib version 2.0 or greater
3. NumPy version 1.10 or greater
4. docopt (any version?)
5. astropy version 1.3 or greater
    
## Sample Run Lines ##
Enter into the root directory of the repository, and type:

```
python3 run_system.py --system=base_systems/system_kep414.fits --dt=30 --run_time_total=864000
```

This line initializes a planetary system like Kepler 414, and has a star that is *very* spotty with a 
fairly quick rotation period.  It will run for 10 days of simulated time, and it will output an obseration
every 30 seconds.  By default, all simulations start at Jan. 1, 2016 at midnight and evolve forward in time.
This run will spit out two files: 

1. kep414.txt (a 2-column CSV file of lightcurve data)
2. state_kep414.fits (a .fits file contaning the evolved state of this system)

If you want to see your lightcurve, you can type:
```
python3 plot_system.py --file=kep414.txt
```

If you want to take observations of the same system later down the line, you can also restart a system based
off of the state fits files that it outputs.  For example, you could do:
```
python3 run_system.py --load=state_kep414.fits --load_date=2016,2,1,0,0,0 --dt=30 --run_time_total=864000
```
This will load the system that was output from the previous run, advance it to its state at February 1, 2016 at midnight,
and then continue running, observing every 30 seconds.  In the end, it will append its data on to kep414.txt.  If kep414.txt
is no longer in the local directory, it will just make a new file with the data from this restarted run.  You can plot the
outputs as you did bfore, and also note that this will update the state_kep414.fits file to be as it is at the end of the restart run.

## Future Improvements to make ##
Having the ability to make eclipsing binaries would be great, but that's just totally not built into this right now.

Limb darkening causes a lot of the shape of lightcurves, and right now there's no accounting for limb darkening.