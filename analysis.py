import numpy as np
import matplotlib.pyplot as plt
plt.style.use('ggplot')
import datetime

s_per_day = 86400.

class SystemAnalyzer:

    def __init__(self, input_file):
        self.file_name = input_file
        self.light = np.genfromtxt(input_file, skip_header=1, usecols=1, delimiter=',')
        self.times = np.zeros_like(self.light)
        with open(input_file, 'r') as f:
            for i, line in enumerate(f):
                if i == 0:
                    continue
                elif i == 1:
                    self.start_date = float(line.split(',')[0])
                date = float(line.split(',')[0])
                self.times[i-1] = date - self.start_date

    def plot(self, fig_name=None, dpi=300, sys_name="", radius=None):
        fig = plt.figure(figsize=(10, 7.5))
        ax = fig.add_subplot(1,1,1)

        from matplotlib.ticker import FormatStrFormatter
        ax.yaxis.set_major_formatter(FormatStrFormatter('%.7f'))

        y_points = self.light/self.light.mean()
        ax.plot(self.times, y_points, '.', ms=1, color='k')
        ax.set_xlabel('Time (days after {})'.format(self.start_date))
        ax.set_ylabel('Normalized Flux')
        ax.set_xlim(np.min(self.times), np.max(self.times))

        title = 'System name: {:s};'.format(sys_name)
        if radius != None:
            title += ' Star radius = {:.3g}'.format(radius) + r'$R_{\odot}$'

        plt.title(title)
            

        if fig_name == None:
            fig_name = self.file_name.split('.txt')[0] + '.png'

        plt.savefig(fig_name, dpi=dpi, bbox_inches='tight')
