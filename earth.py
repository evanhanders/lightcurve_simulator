import system
import analysis
sys_name = 'earth'
file_name = '{:s}.txt'.format(sys_name)

M_earth = 5.974e27 #g
R_earth = 6.378e8  #cm
T_earth = 1.004*86400*365 #s

this_system = system.PlanetarySystem(start_time=(2010,1,1,0,0,0))
this_system.add_planet(M_earth, R_earth, T_earth, 0)
norm = 1e10

dt = 864. #s
n_steps = int(T_earth*3/dt)
this_system.evolve(n_steps=n_steps, dt=dt, noise=True, ncounts_norm=norm)
this_system.save_file(file_name)
this_system.save_state('state_{:s}.fits'.format(sys_name))
this_system.save_system('system_{:s}.fits'.format(sys_name))

