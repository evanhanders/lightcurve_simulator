import system
import analysis

sys_name = 'ads16402B'

s_per_day = 86400

M_earth = 5.974e27 #g
R_earth = 6.378e8  #cm
T_earth = 1.004*86400*365 #s

this_system = system.PlanetarySystem(star_radius=1.174*system.R_sol, spotty=False)
this_system.add_planet(M_earth, 14.778*R_earth, 4.4652968*s_per_day)
norm = 1e7

dt = 864./10 #s
T_max = 9*s_per_day
n_steps = int(T_max/dt)
this_system.evolve(n_steps=n_steps, dt=dt, noise=True, ncounts_norm=norm)
this_system.save_file('{:s}.txt'.format(sys_name))
this_system.save_state('state_{:s}.fits'.format(sys_name))
this_system.save_system('system_{:s}.fits'.format(sys_name))

analyzer = analysis.SystemAnalyzer('{:s}.txt'.format(sys_name))
analyzer.plot()
