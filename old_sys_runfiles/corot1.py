import system
import analysis

sys_name = 'corot1'

s_per_day = 86400

M_earth = 5.974e27 #g
R_earth = 6.378e8  #cm
T_earth = 1.004*86400*365 #s

load = False
load_file = 'state_{:s}.fits'.format(sys_name)
load_date = (2016, 11, 1, 0, 0, 0)
if load:
    this_system = system.PlanetarySystem()
    this_system.load_state(load_file, load_date)
else:
    this_system = system.PlanetarySystem(star_radius=1.11*system.R_sol, spotty=False)
    this_system.add_planet(M_earth, 16.688*R_earth, 1.5089557*s_per_day)
norm = 1e6

dt = 864./10 #s
T_max = 3*s_per_day
n_steps = int(T_max/dt)
this_system.evolve(n_steps=n_steps, dt=dt, noise=True, ncounts_norm=norm)
if load:
    mode = 'a'
else:
    mode = 'w'
this_system.save_file('{:s}.txt'.format(sys_name), flag=mode)
this_system.save_state('state_{:s}.fits'.format(sys_name))
this_system.save_system('system_{:s}.fits'.format(sys_name))

analyzer = analysis.SystemAnalyzer('{:s}.txt'.format(sys_name))
analyzer.plot()
