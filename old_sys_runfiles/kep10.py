import system
import analysis

sys_name = 'kep10'
file_name = '{:s}.txt'.format(sys_name)

s_per_day = 86400

M_earth = 5.974e27 #g
R_earth = 6.378e8  #cm
T_earth = 1.004*86400*365 #s

this_system = system.PlanetarySystem(star_radius=1.065*system.R_sol, spotty=True,
                                     avg_spot_gen=1/(s_per_day), 
                                     min_spot_size=0.3*R_earth, max_spot_size=1.2*R_earth,
                                     rotation_period=30*s_per_day, avg_spot_life=30*s_per_day)
this_system.add_planet(M_earth, 1.47*R_earth, 0.837495*s_per_day)
this_system.add_planet(M_earth, 2.35*R_earth, 45.2948*s_per_day)
norm = 2e8

dt = 864./10 #s
T_max = 2*45.2948*s_per_day
n_steps = int(T_max/dt)
this_system.evolve(n_steps=n_steps, dt=dt, noise=True, ncounts_norm=norm)
this_system.save_file(file_name)
this_system.save_state('state_{:s}.fits'.format(sys_name))
this_system.save_system('system_{:s}.fits'.format(sys_name))

analyzer = analysis.SystemAnalyzer(file_name)
analyzer.plot()
