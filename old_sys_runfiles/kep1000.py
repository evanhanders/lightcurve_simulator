import system
import analysis

sys_name = 'kep1000'
file_name = '{:s}.txt'.format(sys_name)

s_per_day = 86400

M_earth = 5.974e27 #g
R_earth = 6.378e8  #cm
T_earth = 1.004*86400*365 #s

this_system = system.PlanetarySystem(star_radius=1.51*system.R_sol)

scratch = True
if scratch:
    this_system.add_planet(M_earth, 4.76*R_earth, 120*s_per_day, 0)
else:
    this_system.load_state('state_kep1000.txt', (2017,3,1,0,0,0))

norm = 1e8
dt = 864. #s
T_max = 242*s_per_day
n_steps = int(T_max*1.1/dt)
this_system.evolve(n_steps=n_steps, dt=dt, noise=True, ncounts_norm=norm)
if scratch:
    this_system.save_file(file_name, flag='w')
else:
    this_system.save_file(file_name, flag='a')
this_system.save_state('state_{:s}.fits'.format(sys_name))
this_system.save_system('system_{:s}.fits'.format(sys_name))

analyzer = analysis.SystemAnalyzer(file_name)
analyzer.plot()
