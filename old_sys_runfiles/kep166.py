import system
import analysis

sys_name = 'kep166'
file_name = '{:s}.txt'.format(sys_name)

s_per_day = 86400

M_earth = 5.974e27 #g
R_earth = 6.378e8  #cm
T_earth = 1.004*86400*365 #s

this_system = system.PlanetarySystem(star_radius=0.7*system.R_sol)
this_system.add_planet(M_earth, 2.27*R_earth, 7.6503*s_per_day, 0)
this_system.add_planet(M_earth, 2.4*R_earth, 34.2603*s_per_day, 0)
this_system.add_planet(M_earth, 1.69*R_earth, 1.5540*s_per_day, 0)
norm = 1e10

dt = 864./10 #s
T_max = 34.2603*s_per_day
n_steps = int(T_max*2/dt)
this_system.evolve(n_steps=n_steps, dt=dt, noise=True, ncounts_norm=norm)
this_system.save_file(file_name)
this_system.save_state('state_{:s}.fits'.format(sys_name))
this_system.save_system('system_{:s}.fits'.format(sys_name))

analyzer = analysis.SystemAnalyzer(file_name)
analyzer.plot()
