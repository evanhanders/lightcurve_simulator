import system
import analysis

sys_name = 'kep223'
file_name = '{:s}.txt'.format(sys_name)

s_per_day = 86400

M_earth = 5.974e27 #g
R_earth = 6.378e8  #cm
T_earth = 1.004*86400*365 #s

this_system = system.PlanetarySystem(star_radius=1.72*system.R_sol)
this_system.add_planet(M_earth, 2.99*R_earth, 7.38*s_per_day, 0)
this_system.add_planet(M_earth, 3.44*R_earth, 9.84*s_per_day, 0)
this_system.add_planet(M_earth, 5.24*R_earth, 14.8*s_per_day, 0)
this_system.add_planet(M_earth, 4.60*R_earth, 19.7*s_per_day, 0)
norm = 1e9

dt = 864./10 #s
T_max = 2*19.7*s_per_day
n_steps = int(T_max/dt)
this_system.evolve(n_steps=n_steps, dt=dt, noise=True, ncounts_norm=norm)
this_system.save_file(file_name)
this_system.save_state('state_{:s}.fits'.format(sys_name))
this_system.save_system('system_{:s}.fits'.format(sys_name))

analyzer = analysis.SystemAnalyzer(file_name)
analyzer.plot()
