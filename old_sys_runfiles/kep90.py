import system
import analysis

sys_name = 'kep90'
file_name = '{:s}.txt'.format(sys_name)

s_per_day = 86400

M_earth = 5.974e27 #g
R_earth = 6.378e8  #cm
T_earth = 1.004*86400*365 #s

this_system = system.PlanetarySystem(star_radius=1.2*system.R_sol)
this_system.add_planet(M_earth, 1.31*R_earth, 7*s_per_day, 0)
this_system.add_planet(M_earth, 1.19*R_earth, 8.96*s_per_day, 0)
this_system.add_planet(M_earth, 2.9*R_earth, 62.8*s_per_day, 0)
this_system.add_planet(M_earth, 2.7*R_earth, 94.1*s_per_day, 0)
this_system.add_planet(M_earth, 2.9*R_earth, 125*s_per_day, 0)
this_system.add_planet(M_earth, 7.9*R_earth, 210*s_per_day, 0)
this_system.add_planet(M_earth, 11.1*R_earth, 335*s_per_day, 0)
norm = 1e10

dt = 864. #s
T_max = 335*s_per_day
n_steps = int(T_max*2/dt)
this_system.evolve(n_steps=n_steps, dt=dt, noise=True, ncounts_norm=norm)
this_system.save_file(file_name)
this_system.save_state('state_{:s}.fits'.format(sys_name))
this_system.save_system('system_{:s}.fits'.format(sys_name))

analyzer = analysis.SystemAnalyzer(file_name)
analyzer.plot()
