"""
A script which creates a simple plot of a lightcurve given an input file

Usage:
    plot_system.py [options]

Options:
   --file=<file_name>       A .txt file containing lightcurve data
   --fig_name=<name>        The name of the output figure, optional
   --radius=<star_radius>   The radius of the star, in solar units
   --sys_name=<name>        The name/number of the planetary system
"""
from docopt import docopt
import analysis
args = docopt(__doc__)

data_file    = args['--file']
fig_name     = args['--fig_name']
radius       = float(args['--radius'])
sys_name     = args['--sys_name']

if data_file == None:
    raise Exception("Must specify data file")

analyzer = analysis.SystemAnalyzer(data_file)
analyzer.plot(fig_name=fig_name, sys_name=sys_name, radius=radius)
