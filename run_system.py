"""
A script which evolves a planetary system and produces mock lightcurve
observations of that system.  There are two main running modes:

1. Run a new system from scratch, and produce a lightcurve
2. Evolve a system which already exists, and append to its existing lightcurve
    (or, if that lightcurve doesn't exist for some reason, make a new one)

Usage:
    run_system.py [options]
    run_system.py --system=<sys_file_name> --dt=<timestep> --run_time_total=<time>
    run_system.py --load=<load_file> --load_date=<date> --dt=<timestep> --run_time_total=<time>
    run_system.py --load=<load_file> --load_date=<date> --dt=<timestep> --run_time_total=<time> --norm=<norm>

Options:
   --system=<file_name>     A .fits file containing info about the system.
   --out_file=<file_name>   A .txt file name, which output is saved to. Optional.


   --load=<file_name>       A .fits file containing an evolved system.
   --load_date=<date>       The time at which to load a system and start evolving it again
                            (fmt: yr,month,day,hr,min,s) [default: 2017,1,1,0,0,0]

    --dt=<timestep>         The time between observations, in seconds [default: 86.4]
    --run_time_total=<time> The total length of time to observe for, in seconds [default: 864000]
    --norm=<counts>         The number of counts observed from star.  Lower = noise is more prominent [default: 1e8]
"""
from docopt import docopt
import system
args = docopt(__doc__)

sys_file    = args['--system']
out_file    = args['--out_file']

load_file   = args['--load']
load_date   = tuple([int(a) for a in args['--load_date'].split(',')])

dt          = float(args['--dt'])
run_time    = float(args['--run_time_total'])
norm        = float(args['--norm'])
n_steps     = int(run_time/dt)

if load_file == None or load_date == None:
    try:
        if sys_file == None:
            raise Exception("Must specify system file to run from")
        my_system = system.PlanetarySystem(fits_file=sys_file)
        my_system.evolve(n_steps=n_steps, dt=dt, noise=True, ncounts_norm=norm)
        if out_file == None:
            out_file = '{:s}.txt'.format(sys_file.split('system_')[1].split('.fits')[0])
        my_system.save_file(out_file, flag='w')
        my_system.save_state('state_{:s}.fits'.format(out_file.split('.txt')[0]))
    except:
        raise Exception("Something went wrong trying to run the base system")
else:
    if load_file == None or load_date == None:
        raise Exception("Must specify load file AND load date")
    try:
        my_system = system.PlanetarySystem()
        my_system.load_state(load_file, load_date)
        my_system.evolve(n_steps=n_steps, dt=dt, noise=True, ncounts_norm=norm)
        if out_file == None:
            out_file = '{:s}.txt'.format(load_file.split('state_')[1].split('.fits')[0])
        my_system.save_file(out_file, flag='a')
        my_system.save_state('state_{:s}.fits'.format(out_file.split('.txt')[0]))
    except:
        raise Exception("Something went wrong re-running the system")




