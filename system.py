import datetime
import numpy as np
import matplotlib.pyplot as plt
from astropy.io import fits
plt.style.use('ggplot')

M_sol   = 1.989e33 #g
M_earth = 5.972e27 #g

R_sol   = 6.957e10 #cm
R_earth = 6.371e8  #cm

L_sol   = 3.839e33 #erg/s

AU      = 1.496e13 #cm

sec_day = 86400    #seconds / day

N_PTS   = 1000

G = 6.67259e-8 #cm^3/g/s^2

def MJD(full_date):
    """
    Calculates and returns MJD. Note: Can only calculate dates
    between 2010 and 2017, as implemented.  Can be updated in the future
    to more robustly account for leap years.

    Inputs:
        full_date: a datetime.datetime object

    Outputs:
        A float number, containing the correct MJD
    """
    year_base = dict()
    year_base[2010] = 2455197.5
    year_base[2011] = 2455562.5
    year_base[2012] = 2455927.5
    year_base[2013] = 2456293.5
    year_base[2014] = 2456658.5
    year_base[2015] = 2457023.5
    year_base[2016] = 2457388.5
    year_base[2017] = 2457754.5

    # See if date is "valid"
    year = full_date.year
    if year not in year_base.keys():
        raise Exception("The JD base for year {} is unknown".format(year))

    # Calclate JD
    start_time = datetime.datetime(year, 1, 1, 0, 0, 0)
    delta = full_date - start_time
    add = delta.days + delta.seconds/sec_day

    # Convert JD to MJD
    return year_base[year] + add - 2400000.5


class CelestialBody:
    """
    A simple class which contains information about a planet or star

    Attributes:
    -----------
        mass        - the mass of the object, in g
        radius      - The radius of the object, in cm
        period      - The amount of time it takes for the planet to orbit, in s
        inclination - The inclination of the planet's orbit off the x-y plane, in radians
        vis_lum     - The visible luminosity of the object, in erg/s
    """
    def __init__(self, mass, radius, period, inclination=0, vis_luminosity=0):
        self.mass         = mass
        self.radius       = radius
        self.period       = period
        self.inclination  = inclination
        self.vis_lum      = vis_luminosity


class Planet(CelestialBody):
    """
    A child class of CelestialBody which sets up a planet in a circular orbit around
    a star at x, y, z = 0, 0, 0

    Attributes:
    -----------
        All attributes of the CelestialBody class
        smj         - The semimajor axis of the planet's orbit, in cm
        max_inc     - The maximum inclination angle allowed so that the planet still
                      partially transits the star.
        pos         - A 3-element NumPy array of the x, y, z position of the planet.
    """
    
    def __init__(self, star_mass, star_radius, *args, **kwargs):
        """
        Initializes a planet object, calculating its initial position.  Note
        that a planet whose inclination is specified as 1 will have an inclination
        value of self.max_inc, which is just above the inclination values that are
        measurable by the star.

        Inputs:
        ------
            star_mass   - the mass of the star the planet is orbiting, in g
            star_radius - The radius of the star the planet is orbiting, in cm
            *args, **kwargs - The arguments, keyword arguments of parent class __init__
        """
        super(Planet, self).__init__(*args, **kwargs)
        self.smj = np.cbrt((star_mass+self.mass)*(self.period)**2*G/(4*np.pi**2))
        self.max_inc = np.arcsin((self.radius+star_radius)/self.smj)

        self.inclination *= self.max_inc

        theta = np.random.rand()*2*np.pi
        self.pos = np.zeros(3)
        self.pos[0] = self.smj * np.cos(theta) * np.cos(self.inclination)
        self.pos[1] = self.smj * np.sin(theta)
        self.pos[2] = self.smj * np.cos(theta) * np.sin(self.inclination)


    def timestep(self, dt):
        """
        Evolves the position of the planet by a specified timestep

        Inputs:
        -------
            dt  - A timestep to take, in seconds
        """

        # Calculate the "theta" value of the planet's current position, in radians
        theta   = np.arctan(np.abs(self.pos[1]/np.sqrt(self.pos[0]**2 + self.pos[2]**2)))
        # We need to tailor the outputs of arctan into the 0-->2 pi range we care about
        if self.pos[1] < 0 and self.pos[0] < 0:
            theta += np.pi
        elif self.pos[1] >= 0 and self.pos[0] < 0:
            theta = np.pi - theta
        elif self.pos[1] < 0 and self.pos[0] >= 0:
            theta = 2*np.pi - theta

        # Figure out what fraction of the planet's period dt is, then step that
        # much of a fraction through its orbit.
        angle_step = 2*np.pi*dt/self.period
        theta   += angle_step
        theta   %= 2*np.pi
        self.pos[0] = self.smj * np.cos(theta) * np.cos(self.inclination)
        self.pos[1] = self.smj * np.sin(theta) 
        self.pos[2] = self.smj * np.cos(theta) * np.sin(self.inclination)

class Sunspot:
    """
    A class which stands for a sunspot on a given star, and contains all
    of the important information about that sunspot.

    Attributes:
    ----------
        lifetime    - The total lifetime of the sunspot, in seconds
        radius      - The maximum radius of the sunspot, in cm
        longitude   - The longitudinal position of the spot on the star, in radians
        latitude    - The latitudinal position of the spot on the star, in radians
        time        - The time that has elapsed in the spot's lifetime.
        star_radius - The radius of the star that this spot is on, in cm
        app_radius  - The current radius of the star (which changes over time)
        delta_phi   - The angular fraction of the star that the spot takes up
    """
    
    def __init__(self, lifetime, radius, longitude, latitude, star_radius):
        """
        Initializes a sunspot.  All inputs correspond to the class attribute of
        the same name, as defined in the class docstring.
        """
        self.lifetime   = lifetime
        self.radius     = radius
        self.longitude  = longitude
        self.latitude   = latitude
        self.time       = 0
        self.star_radius = star_radius
        self.get_apparent_radius()

    def get_apparent_radius(self):
        """
        Calculates the current size of the sunspot.  Assumes that the size of sunspots
        follows a Gaussian profile as a function of time, where mu is the midway
        point of the spot's life, and sigma is the lifetime / 5 (such that it
        is very small right when it starts existing and before it disappears).
        """
        sigma = self.lifetime / 5.
        mu    = self.lifetime / 2.
        self.app_radius = self.radius * np.exp(-(self.time - mu)**2/(2*sigma**2))
        self.delta_phi = np.arcsin(self.app_radius/self.star_radius)

    def timestep(self, dt):
        """
        Evolve the sunspot forward in time by an amount specified.

        Inputs:
        -------
            dt - The timestep to take, in seconds.
        """
        self.time += dt
        self.get_apparent_radius()

class Star(CelestialBody):
    """
    A child class of CelestialBody which represents a star.  Has the ability
    to keep track of starspots and rotate, but maintains fixed at a fixed
    position of x, y, z = 0, 0, 0.

    Attributes:
    -----------
    rotation -- The rotation period of the star, in s
    spotty   -- If True, this star is a star that generates sunspots
    min_spot_size -- The minimum size of sunspots, in cm
    max_spot_size -- The maximum size of sunspots, in cm
    avg_spot_gen  -- The average frequency of spot generation, in Hz.
                    (e.g., there's a 50/50 shot of a new spot showing up
                    every day if avg_spot_gen = (1/86400) )
    avg_spot_life -- Average lifetime of spots, in seconds
    orientation   -- The star has a set of longitude/latitude coordinates.  
                     This is the angle that the star's longitude = 0 line has 
                     rotated off of the x-axis, in units of radians.
    next_spot_gen -- The time, in seconds, until the next sunspot is generated
    spots         -- A list of Sunspot objects on the star
    base_fluxmap  -- A list containing a 2D map of x, y, and illumination values of
                     the star, as well as the "baseline" flux from the star (see
                     the get_base_fluxmap function).
    fluxmap       -- A 2D map of current fluxes for the star, used to determine how
                     much a planet moving in front of the star blocks its light.
    """

    def __init__(self, rotation, *args, 
                 min_spot_size=0.1*R_earth, max_spot_size=5*R_earth,
                 avg_spot_gen=1./(10*sec_day), avg_spot_life = 10*sec_day,
                 spotty=False, **kwargs):
        """
        Initializes the star object.

        Inputs:
        ------
            All inputs with the same names as class attributes correspond to the
            attribute described in the class docstring.
            *args, **kwargs -- The arguments/keyword args to the parent __init__
        """

        super(Star, self).__init__(*args, **kwargs)
        self.rotation       = rotation
        self.spotty         = spotty
        self.min_spot_size  = min_spot_size
        self.max_spot_size  = max_spot_size
        self.avg_spot_gen   = avg_spot_gen
        self.avg_spot_life  = avg_spot_life
        self.orientation    = 0 
        self.next_spot_gen  = np.random.rand()/self.avg_spot_gen

        self.spots = []
        self.get_base_fluxmap()

        # Add some sunspots to the star, initially, so we don't always start at
        # the perfect normalized flux (if the star is spotty)
        num_start = int(np.ceil(self.avg_spot_life*self.avg_spot_gen))
        if spotty:
            for i in range(num_start):
                self.generate_spot()
            for i, spot in enumerate(self.spots):
                self.spots[i].time = spot.lifetime*np.random.rand()
                self.spots[i].get_apparent_radius()
                

    def timestep(self, dt):
        """
        Evolves the star by a given timestep.  Checks if spots need to be
        generated if it's a spotty star.  Otherwise, just rotates everything.

        Inputs:
        -------
        dt    -- A timestep to advance, in seconds.
        """
        self.orientation += 2*np.pi*dt/self.rotation
        self.orientation %= 2*np.pi

        # Deal with sunspots
        if self.spotty:
            self.next_spot_gen -= dt
            # If it's been a while since a sunspot was generated, make a new one
            if self.next_spot_gen <= 0:
                self.generate_spot()
                self.next_spot_gen = 1./self.avg_spot_gen
            # Loop over all spots and decrease their remaining lifetime
            dead_spots = []
            for i in range(len(self.spots)):
                self.spots[i].timestep(dt)
                if self.spots[i].time >= self.spots[i].lifetime:
                    dead_spots.append(i)
                self.spots[i].get_apparent_radius()
            # Remove spots that have exceeded their lifetime
            for indx in dead_spots:
                self.spots.pop(indx)

    def generate_spot(self):
        """
        Potentitally generates a sunspot (or not), and adds it to the class list.
        Every time this function is called, there's a 50/50 chance of a sunspot
        being created.  The size randomly ranges from the min to the max value,
        and the lifetime ranges from 0.8 - 1.2 * the avg spot lifetime.  The
        longitude is randomly generated between 0 and 2 pi, the latitude is
        randomly generated between - pi/3 to pi/3.
        """
        if np.random.rand() < 0.5:
            spot_size = (self.max_spot_size-self.min_spot_size)*(np.random.rand()) + self.min_spot_size
            spot_life = self.avg_spot_life*(np.random.rand()*0.4 + 0.8)
            longitude = np.random.rand()*2*np.pi
            latitude  = np.random.rand()*2*np.pi/3. - np.pi/3.
            self.spots.append(Sunspot(spot_life, spot_size, longitude, latitude, self.radius))

    def get_base_fluxmap(self):
        """
        Gets the base "fluxmap" of the star.  Each star has a 2D array that
        represents the star, as observed from down the x-axis.  Thus, 'x'
        values are y- values and 'y' values are z-values.

        When the star is unblocked, this map contains a circle of the
        star's radius which contains 0 outside of the star and 1 inside of
        the star.  The sum of 1's on this map is the baseline flux received
        from the star.  Any planet passing in front of the star's face puts
        the corresponding values on this map to zero, thus reducing the flux
        from the baseline.
        """
        x = np.linspace(-self.radius, self.radius, N_PTS)
        y = np.linspace(-self.radius, self.radius, N_PTS)
        xs, ys = np.meshgrid(x, y)
        star = np.ones(xs.shape)
        star[np.where(np.sqrt(xs**2 + ys**2) > self.radius)] = 0
        baseline = np.sum(star)
        self.base_fluxmap = [xs, ys, star, baseline]
        self.fluxmap      = np.copy(star)

    def spots_flux_blocked(self):
        """
        Adjust the fluxmap values based on the presence of sunspots.

        Loops over all sunspots, determines what fraction of the total star's
        flux is now taken up by sunspots, and then returns the fractional percentage
        of the star's light that is left after taking out the sunspot flux.

        Outputs:
        --------
            light_adjust - a number between 0 and 1, which the baseline flux must
                           be multiplied by before subtracting the flux blocked
                           by planets.
        """
        light_adjust = 1
        if self.spotty:
            for i,spot in enumerate(self.spots):
                #Calculate the "observed" longitude of the spot.
                longitude = spot.longitude + self.orientation
                longitude %= 2*np.pi
                longitude -= np.pi/2.

                #If longitude is observable, then account for the lost flux
                if  longitude >= -np.pi/2 - spot.delta_phi and\
                    longitude <=  np.pi/2 + spot.delta_phi:
                    if longitude <= np.pi/2.-spot.delta_phi and longitude >= -np.pi/2+spot.delta_phi:
                        percent_on = 1
                    elif longitude < 0: #sunspot is on the left side of the star, partly in view
                        percent_on = (longitude+(np.pi/2.+spot.delta_phi))/(2*spot.delta_phi)
                    else: #sunspot is on the right side of star, partly in view
                        percent_on = 1-(longitude-(np.pi/2.-spot.delta_phi))/(2*spot.delta_phi)
                    light_adjust -= percent_on*spot.app_radius**2/self.radius**2
        return light_adjust

    def reset_fluxmap(self):
        """ 
        Resets the star's fluxmap back to base, erasing lost flux from planets.
        """
        self.fluxmap = np.copy(self.base_fluxmap[2])

                    
class PlanetarySystem:
    """
    A class which models a planetary system.  The dynamics of the system are simple,
    with the star at the center of the coordinate system (x, y, z) = (0, 0, 0), and
    all of the planets around that star in stable, circular orbits.  Rather than
    updating based on forces, the positions of planets are updated based on
    angular steps through their orbital circles.  While not perfectly accurate,
    this allows for restarting the system at much later times properly, and
    also allows for large (or small) timesteps to be taken accurately.

    Attributes:
    ----------
        star        -- An object of the Star class, the star of the system.
        start_time  -- A datetime object holding a calendar date at which timestep 0 occurs.
        planets     -- A list containing Planet objects of each planet in the system.
        times       -- After evolving, the times at which observations are taken
        lightcurve  -- After evolving, the brightness values corresponding to
                       the times in the 'times' array

    """

    def __init__(self, fits_file=None, start_time=(2016,1,1,0,0,0), star_mass=M_sol, star_radius=R_sol, 
                       rotation_period=sec_day*30, star_luminosity = L_sol,
                       min_spot_size=0.1*R_earth, max_spot_size=5*R_earth,
                       avg_spot_gen = 1./(10*sec_day), avg_spot_life = 10*sec_day,
                       spotty=False):
        '''
        Initializes the system.

        Keyword Arguments:
        -----------------
            start_time      - A six-element tuple containing (year, month, day, hr, min, sec)
            star_mass       - Mass of the star, in grams
            star_radius     - Radius of the star, in cm
            rotation_period - Rotation period of the star, in seconds
            star_luminosity - The luminosity of the star, in erg/s (currently not used)
            min_spot_size   - The min size of sunspots, in cm
            max_spot_size   - The max size of sunspots, in cm
            avg_spot_gen    - The average time between sunspot generation, in s
            avg_spot_life   - The average lifetime of sunspots, in s
            spotty          - If False, the star doesn't have sunspots.
        '''
        if fits_file != None:
            self._load_system(start_time, fits_file)
            return
            
        self.star = Star(rotation_period, star_mass, star_radius, 0, 0, 
                         min_spot_size=min_spot_size, max_spot_size=max_spot_size,
                         avg_spot_gen=avg_spot_gen, avg_spot_life=avg_spot_life,
                         spotty=spotty, vis_luminosity=star_luminosity)
        self.start_time = datetime.datetime(*start_time)
        self.times, self.lightcurve = None, None
        self.planets = []

    def add_planet(self, *args, **kwargs):
        '''
            Adds a planet object to the class list.  Passes through all 
            args and kwargs to the Planet class  __init__ function.
        '''
        planet = Planet(self.star.mass, self.star.radius, *args, **kwargs)
        self.planets.append(planet)

    def save_file(self, file_name, flag='w'):
        """
        Saves self.times and self.lightcurve out to a two-column file, in which
        the times are recorded as MJD, and the flux values are directly recorded.

        Inputs:
        -------
            file_name -- The name of the file, e.g. "system1.txt"
            flag      -- If 'w', make a new file.  If 'a', append to the existing file
        """
        out_file = open(file_name, flag)
        if flag == 'w':
            out_file.write('{:<15s},{:<20s}\n'.format('Time (MJD)', 'Flux (counts)'))
        for t, l in zip(self.times, self.lightcurve):
            out_file.write('{:<15.12f},{:<20.12g}\n'.format(MJD(self.start_time + datetime.timedelta(0, t)), l))
        out_file.close()
            

    def timestep(self, dt=864):
        '''
        Take one timestep through the system, update the positions of planets
        and starspots. 

        Inputs:
        ------
            dt -- The size of the timestep to take, in seconds. Default 864 s
                  is for 100 timesteps / day.
        '''
        self.star.timestep(dt)
        for i, planet in enumerate(self.planets):
            planet.timestep(dt)
        
    def evolve(self, n_steps=1000, dt=864, noise=True, ncounts_norm=1e4):
        """
        Evolve the planetary system through a specified number of timesteps.

        Inputs:
        ------
            n_steps      - The total number of timesteps to take
            dt           - The timestep size to take, in seconds
            noise        - If True, draw data from Poisson statistics.  Otherwise,
                           assume observations are perfect.
            ncounts_norm - The number of counts received when none of the star
                           is blocked by sunspots or planets.  Use a larger
                           value to reduce the relative noise if noise=True
        """
        lightcurve = np.ones(int(n_steps))
        for i in range(int(n_steps)):
            if np.mod(i+1,100) == 0:
                print('timestep {} complete; num spots: {}'.format(i+1, len(self.star.spots)))
            self.timestep(dt=dt)
            counts = self.get_flux_value()*ncounts_norm
            if noise:
                lightcurve[i] = np.random.poisson(counts, 1)
            else:
                lightcurve[i] = counts
        self.times, self.lightcurve = np.arange(n_steps)*dt, lightcurve

    def get_flux_value(self):
        """
        Takes a measurement of the stellar system from along the x-direction.
        Gets the amount of the flux that is blocked as a result of sunspots,
        as well as the amount of flux that's blocked due to planets passing
        between the observer and the star.<Down>
        """
        light_adjust = self.star.spots_flux_blocked()
        modified = False
        for i, planet in enumerate(self.planets):
            # x > 0 is in between us and star, and at least PART of the planet needs
            # to be blocking the star for us to care about it as a transit.
            if planet.pos[0] >= 0 and \
             ((planet.pos[1] >= 0 and planet.pos[1]-planet.radius < self.star.radius) or \
              (planet.pos[1] <= 0 and planet.pos[1]+planet.radius > -self.star.radius) ):

                    planet_indices = np.sqrt((self.star.base_fluxmap[0] - planet.pos[1])**2 \
                                           + (self.star.base_fluxmap[1] - planet.pos[2])**2)\
                                     < planet.radius
                    self.star.fluxmap[planet_indices] = 0
                    modified = True
        if not modified:
            # If there are no planets, only account for sunspots
            return light_adjust
        else:
            # Account for planets and sunspots
            num_counts_below = self.star.base_fluxmap[-1] - np.sum(self.star.fluxmap)
            num_counts = self.star.base_fluxmap[-1]*light_adjust - num_counts_below
            self.star.reset_fluxmap()
            return num_counts/self.star.base_fluxmap[-1]

    def save_system(self, file_name):
        hdr = fits.Header()
        #Star info
        hdr['mass']        = self.star.mass
        hdr['radius']      = self.star.radius
        hdr['lum']  = self.star.vis_lum
        hdr['rotation']  = self.star.rotation
        hdr['spotty']           = self.star.spotty
        hdr['mins']    = self.star.min_spot_size
        hdr['maxs']    = self.star.max_spot_size
        hdr['sgen']     = self.star.avg_spot_gen
        hdr['slife']    = self.star.avg_spot_life

        #time
        time_now = self.start_time + datetime.timedelta(0, self.times[-1])
        hdr['year']     = time_now.year
        hdr['month']    = time_now.month
        hdr['day']      = time_now.day
        hdr['hour']     = time_now.hour
        hdr['minute']   = time_now.minute
        hdr['second']   = time_now.second

        planet_info = []
        for p in self.planets:
            planet_info.append([p.mass, p.radius, p.period, p.inclination/p.max_inc, p.vis_lum])
        planet_info = np.array(planet_info)
        hdu = fits.PrimaryHDU(planet_info, header=hdr)
        hdu.writeto(file_name, overwrite=True)

    def _load_system(self, date, file_name):
        f = fits.open(file_name)[0]
        hdr = f.header

        #Get star info
        star_mass       = hdr['mass']
        star_radius     = hdr['radius']
        star_lum        = hdr['lum']
        rotation_period = hdr['rotation']
        min_spot_size   = hdr['mins']
        max_spot_size   = hdr['maxs']
        avg_spot_gen    = hdr['sgen']
        avg_spot_life   = hdr['slife']
        spotty          = hdr['spotty']
        self.__init__(fits_file=None, start_time=date, star_mass=star_mass, star_radius=star_radius,
                      rotation_period=rotation_period, star_luminosity=star_lum,
                      min_spot_size=min_spot_size, max_spot_size=max_spot_size,
                      avg_spot_gen=avg_spot_gen, avg_spot_life=avg_spot_life,
                      spotty=spotty)
        
        # Add planets
        planets = f.data
        for i in range(planets.shape[0]):
            p = planets[i,:]
            self.add_planet(*p[:4], vis_luminosity=p[4])


    def save_state(self, file_name):
        """
        Save the state of the planetary system to a file so that it
        can be loaded back in later
        TODO: Turn this into a .fits file, where the header contains all
        stellar information and the HDU contains the planets.

        Inputs:
        ------
            file_name -- The name of the file to save the state to
        """

        hdr = fits.Header()
        #Star info
        hdr['mass']        = self.star.mass
        hdr['radius']      = self.star.radius
        hdr['lum']  = self.star.vis_lum
        hdr['rotation']  = self.star.rotation
        hdr['spotty']           = self.star.spotty
        hdr['mins']    = self.star.min_spot_size
        hdr['maxs']    = self.star.max_spot_size
        hdr['sgen']     = self.star.avg_spot_gen
        hdr['slife']    = self.star.avg_spot_life

        #time
        time_now = self.start_time + datetime.timedelta(0, self.times[-1])
        hdr['year']     = time_now.year
        hdr['month']    = time_now.month
        hdr['day']      = time_now.day
        hdr['hour']     = time_now.hour
        hdr['minute']   = time_now.minute
        hdr['second']   = time_now.second

        planet_info = []
        for p in self.planets:
            planet_info.append([p.mass, p.radius, p.period, p.inclination/p.max_inc, p.vis_lum,
                                p.smj, p.pos[0], p.pos[1], p.pos[2]])
        planet_info = np.array(planet_info)
        hdu = fits.PrimaryHDU(planet_info, header=hdr)
        hdu.writeto(file_name, overwrite=True)

    def load_state(self, file_name, date, advance=False):
        """
        Loads the state of a planet from a file.  Restarts simulations at some
        later date than where the system was

        Inputs:
        ------
            file_name   - The name of the file to load data from.
            date        - The date to restart simulations on.
        """
        f = fits.open(file_name)[0]
        hdr = f.header

        #Get star info
        star_mass       = hdr['mass']
        star_radius     = hdr['radius']
        star_lum        = hdr['lum']
        rotation_period = hdr['rotation']
        min_spot_size   = hdr['mins']
        max_spot_size   = hdr['maxs']
        avg_spot_gen    = hdr['sgen']
        avg_spot_life   = hdr['slife']
        spotty          = hdr['spotty']
        self.__init__(start_time=date, star_mass=star_mass, star_radius=star_radius,
                      rotation_period=rotation_period, star_luminosity=star_lum,
                      min_spot_size=min_spot_size, max_spot_size=max_spot_size,
                      avg_spot_gen=avg_spot_gen, avg_spot_life=avg_spot_life,
                      spotty=spotty)
        
        #Fast-forward to current date
        year, month, day = hdr['year'], hdr['month'], hdr['day']
        hour, minute, second = hdr['hour'], hdr['minute'], hdr['second']
        old_time = datetime.datetime(year, month, day, hour, minute, second)
        time_diff = self.start_time - old_time
        seconds   = time_diff.days*sec_day
        if seconds < 0:
            raise Exception("Cannot do follow-up observations at an earlier date than the end of the previous run")

        planets = f.data
        for i in range(planets.shape[0]):
            p = planets[i,:]
            self.add_planet(*p[:4], vis_luminosity=p[4])
            self.planets[-1].smj = p[5]
            self.planets[-1].pos = np.array(p[6:])
            self.planets[-1].timestep(seconds)
